﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using serviciosWeb.Models;

namespace serviciosWeb.Controllers
{
    public class LibrosController : ApiController
    {
        private MoviesEntities db = new MoviesEntities();

        // GET api/Libros
        public IEnumerable<Libro> GetLibroes()
        {
            return db.Libroes.AsEnumerable();
        }

        // GET api/Libros/5
        public Libro GetLibro(int id)
        {
            Libro libro = db.Libroes.Find(id);
            if (libro == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return libro;
        }

        // PUT api/Libros/5
        public HttpResponseMessage PutLibro(int id, Libro libro)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != libro.Id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(libro).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Libros
        public HttpResponseMessage PostLibro(Libro libro)
        {
            if (ModelState.IsValid)
            {
                db.Libroes.Add(libro);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, libro);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = libro.Id }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Libros/5
        public HttpResponseMessage DeleteLibro(int id)
        {
            Libro libro = db.Libroes.Find(id);
            if (libro == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Libroes.Remove(libro);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, libro);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}